import setuptools

with open("README.md", "r") as fh:
      long_description = fh.read()

setuptools.setup(
      name="mindsphere_api",
      version="0.0.2",
      author="Petr Dvoracek",
      author_email="pedro.dvoracek@gmail.com",
      description="short description.",
      long_description=long_description,
      long_description_content_type="text/markdown",
      url="https://gitlab.com/PetrDvoracek/mindsphere-api",
      packages=setuptools.find_packages(),
      classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
      ],
)